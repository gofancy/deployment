# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed

- versioning: `GitVersionProvider` always used the bleeding qualifier if the Git repository contains no tags, even if
  head is on a default branch. It now correctly uses the snapshot qualifier in such cases.
- The plugin were accessing their extension's properties before the build script was evaluated, causing custom
  configurations to be ignored. This now works as expected.

## [0.1.0]

Because the plugins now use a different group we can restart versioning without breaking references to the old
artifacts.

### Changed

- the plugins are now located under the groups `wtf.gofancy.gradle.versioning` and `wtf.gofancy.gradle.publishing`
- the main library providing the plugins is now located under the group `wtf.gofancy.gradle.deployment2`

## [1.1.0]

This version is no longer available (see description of version 0.2.0).

### Added

- a default version descriptor if the version provider fails and log messages to help the user solve the issue
- detect correct branch even if on detached head
- log the configured version

### Changed

- `versioning` plugin no longer fails to apply in empty git repositories
- credential properties were moved to `wtf.gofancy.deployment.mavenUser` and `wtf.gofancy.deployment.mavenToken`
- `publishing` plugin no longer auto applies `maven-publish` plugin
- `publishing` plugin no longer depends on `versioning` plugin
- improvements to already present log messages

### Fixed

 - correctly determine version when in CI tag pipeline
 - `publishing` plugin now skips repository setup when required properties are not present
