# Garden of Fancy Deployment Plugin

The plugins located in this project aim to ease the deployment of artifacts to our Maven repository.

Even though this project was primarily created for internal usage, it should is configurable enough to work for others as well.

## Applying the Plugins

##### `settings.gradle.kts`

Because the plugins are primarily for internal usage, they are intentionally not published to the Gradle plugin portal. They are only available on our own repository, which needs to be added manually.

```kotlin
pluginManagement {
    repositories {
        exclusiveContent {
            forRepository {
                maven {
                    name = "Garden of Fancy Release Repo"
                    url = uri("https://maven.gofancy.wtf/releases")
                    //        "https://maven.gofancy.wtf/snapshots"
                }
            }
            filter {
                includeGroupAndSubgroups("wtf.gofancy.gradle")
            }
        }
        gradlePluginPortal()
    }
}
```

##### `build.gradle.kts`

```kotlin
plugins {
    id("wtf.gofancy.gradle.versioning") version "<version>"
    id("wtf.gofancy.gradle.publishing") version "<version>"
}
```

Both plugins can be used standalone, though the `publishing` plugin benefits from having the `versioning` plugin applied
as well. See the configuration options of the `publishing` plugin for more information on this.

## The `versioning` Plugin

This plugin examines the state of the project's enclosing Git repository to autoconfigure the project's version based on it.

### Version Policy

`M.m.p-qualifier-identifier`

The base version (M.m.p) is always equal to the latest (nearest) annotated Git tag (the tag produced by calling `git describe`). Therefor, if this plugin is applied, it expects annotated tags to always have a name in the form of a valid [semver](https://semver.org) version descriptor. The only exception is that the tag name is allowed to be prefixed with `v`, which prefix then gets removed automatically.

The qualifier describes what kind of repository this artifact belongs to. There are only three possible values for this field:
 - *empty*: this version belongs to a release artifact and has therefor no further qualification. An artifact is a release, if the distance of the current `HEAD` to the nearest annotated tag is 0. Therefor if the current commit is tagged.
 - **SNAPSHOT**: this version belongs to a snapshot artifact. Snapshot versions get used for the projects default branch (this is configurable, see below) and are meant to be deployed on every push to this default branch. This fits a feature based Git workflow, where pushes to the master only happen when a complete feature gets merged.
 - **BLEEDING**: this version belongs to a bleeding artifact. Every version that is neither a release nor a snapshot is automatically of type bleeding. This qualifier will therefor always be used when the current `HEAD` is not on the default branch. Bleeding artifacts are meant to be deployed on every single push and to override a previous deployment if one is present. This allows to test the latest artifact, if desired.

The identifier is different for each type and is used to determine the exact artifact:
  - release: releases may use the identifier for a typical [semver](https://semver.org) pre-release identifier, like for example `alpha1`.
  - snapshot: a snapshot artifact may have no identifier, in which case it refers to the latest snapshot. If an identifier is provided it resembles the hash of the git commit (in short form) for which this artifact was build.
  - bleeding: bleeding artifacts always override the previous artifact on the same branch. Therefor the identifier is only used to describe the branch for which this artifact was build.

### Configuration Options

The plugin provides its own extension, which can be configured to change the used `VersionProvider`. 

```kotlin
versioning {
    versionProvider.set(GitVersionProvider(project))
}
```

The constructor parameters of `GitVersionProvider` allow to set a different list of "default" branches. This list will then be used to determine if the version is a snapshot or not. 

Alternatively a `FixedVersionProvider` can be used. This may be useful for projects that want to use the `publishing` plugin but do not want their version to be set based on the Git repository.

## The `publishing` Plugin

This plugin configures the projects publishing repositories based on the version set by the `versioning` plugin. For each version qualifier a different maven repository can be defined, which will then be activated, if the version happens to be of this qualifier.

The advantage of this is that in CI/CD pipelines it is enough to just call `gradlew publish` and the artifact will only be published to the correct repository as the other are not even defined. This greatly simplifies the definition of CI/CD jobs.

### Configuration Options

By default, the plugin will only enable the release and snapshot repository (which effectively disables deployment of bleeding versions). 

The convention (default value) of `usedVersionQualifier` depends on whether the `versioning` plugin was applied as well.
If it was, it will be the qualifier of the current version (see [version policy](#version-policy)). If it was not, the
release qualifier will be used, this means the release repository will always be added and the CI job needs rules to 
only publish on a tag. 

```kotlin
deployment {
    usedVersionQualifier.set(VersionQualifier.RELEASE)
    
    deployReleaseVersions.set(true)
    deploySnapshotVersions.set(true)
    deployBleedingVersions.set(false)

    qualifierToRepositoryUrl.set(mapOf(
        VersionQualifier.RELEASE to "https://maven.gofancy.wtf/releases",
        VersionQualifier.SNAPSHOT to "https://maven.gofancy.wtf/snapshots",
        VersionQualifier.BLEEDING to "https://maven.gofancy.wtf/bleeding"
    ))
}
```

### Repository Authentication

The plugin uses the Gradle properties `wtf.gofancy.deployment.mavenUser` and `wtf.gofancy.deployment.mavenToken` as
credentials for the added maven repository. If one of the properties is not supplied the plugin will not add any 
repository (this usually happens when building the project locally).
