import java.util.*

plugins {
    `java-gradle-plugin`
    `maven-publish`

    alias(libs.plugins.kotlin.jvm)

    alias(libs.plugins.licenser)

    alias(libs.plugins.versioning)
    alias(libs.plugins.publishing)
}

java {
    withSourcesJar()
}

license {
    header(project.file("NOTICE").let { if (it.exists()) it else project.rootProject.file("NOTICE") })
    include("**/*.kt")

    ext["year"] = "${Calendar.getInstance().get(Calendar.YEAR)}"
    ext["name"] = "Garden of Fancy"
    ext["email"] = "dev@gofancy.wtf"
    ext["app"] = "Gradle Deployment Plugin"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.bundles.kotlin)

    implementation(libs.jgit)
}

group = "wtf.gofancy.gradle.deployment2"

gradlePlugin {
    val versioning by plugins.creating {
        id = "wtf.gofancy.gradle.versioning"
        implementationClass = "wtf.gofancy.deployment.versioning.GofancyVersioningPlugin"
    }
    val publishing by plugins.creating {
        id = "wtf.gofancy.gradle.publishing"
        implementationClass = "wtf.gofancy.deployment.publishing.GofancyPublishingPlugin"
    }
}
