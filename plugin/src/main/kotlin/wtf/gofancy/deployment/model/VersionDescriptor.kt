/*
 * This file is part of Gradle Deployment Plugin, licensed under the MIT License
 *
 * Copyright (c) 2024 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.deployment.model

data class VersionDescriptor(
    val major: Int,
    val minor: Int,
    val patch: Int,
    val qualifier: VersionQualifier,
    val identifier: String,
) {

    constructor(version: String, qualifier: VersionQualifier, identifier: String) : this(
        version.split(".").map(String::toInt),
        qualifier, identifier
    )

    constructor(versionParts: List<Int>, qualifier: VersionQualifier, identifier: String) : this(
        major = versionParts[0],
        minor = versionParts[1],
        patch = versionParts[2],
        qualifier, identifier
    )

    override fun toString(): String {
        val base = "$major.$minor.$patch"
        val ext = "${qualifier.displayString}-$identifier".removePrefix("-").removeSuffix("-")

        return if (ext.isNotBlank()) "$base-$ext" else base
    }
}

enum class VersionQualifier(val displayString: String) {
    RELEASE(""),
    SNAPSHOT("SNAPSHOT"),
    BLEEDING("BLEEDING");
}
