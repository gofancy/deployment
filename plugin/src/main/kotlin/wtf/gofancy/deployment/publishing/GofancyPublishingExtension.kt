/*
 * This file is part of Gradle Deployment Plugin, licensed under the MIT License
 *
 * Copyright (c) 2024 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.deployment.publishing

import org.gradle.api.Project
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import wtf.gofancy.deployment.model.VersionQualifier
import wtf.gofancy.deployment.versioning.GofancyVersioningExtension

interface GofancyPublishingExtension {

    val usedVersionQualifier: Property<VersionQualifier>

    val deployReleaseVersions: Property<Boolean>
    val deploySnapshotVersions: Property<Boolean>
    val deployBleedingVersions: Property<Boolean>

    val qualifierToRepositoryUrl: MapProperty<VersionQualifier, String>

    fun getDeployableVersionQualifiers(): Set<VersionQualifier> {
        val qualifiers = mutableSetOf<VersionQualifier>()

        if (deployReleaseVersions.get()) qualifiers.add(VersionQualifier.RELEASE)
        if (deploySnapshotVersions.get()) qualifiers.add(VersionQualifier.SNAPSHOT)
        if (deployBleedingVersions.get()) qualifiers.add(VersionQualifier.BLEEDING)

        return qualifiers.toSet()
    }
}

internal fun GofancyPublishingExtension.applyConventionsFor(target: Project) {
    this.usedVersionQualifier.convention(
        target.extensions.findByType(GofancyVersioningExtension::class.java)
            ?.versionProvider
            ?.get()
            ?.createVersionDescriptor()
            ?.qualifier ?: VersionQualifier.RELEASE
    )

    this.deployReleaseVersions.convention(true)
    this.deploySnapshotVersions.convention(true)
    this.deployBleedingVersions.convention(false)

    this.qualifierToRepositoryUrl.convention(
        mapOf(
            VersionQualifier.RELEASE to "https://maven.gofancy.wtf/releases",
            VersionQualifier.SNAPSHOT to "https://maven.gofancy.wtf/snapshots",
            VersionQualifier.BLEEDING to "https://maven.gofancy.wtf/bleeding"
        )
    )
}
