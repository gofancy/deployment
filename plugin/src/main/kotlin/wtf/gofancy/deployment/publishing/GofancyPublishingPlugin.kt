/*
 * This file is part of Gradle Deployment Plugin, licensed under the MIT License
 *
 * Copyright (c) 2024 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.deployment.publishing

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.publish.PublishingExtension
import java.net.URI

class GofancyPublishingPlugin : Plugin<Project> {

    companion object {

        private const val EXTENSION_NAME = "deployment"

        private const val PROP_USER = "wtf.gofancy.deployment.mavenUser"
        private const val PROP_TOKEN = "wtf.gofancy.deployment.mavenToken"
    }

    override fun apply(target: Project) {
        val ext = target.extensions.create(EXTENSION_NAME, GofancyPublishingExtension::class.java)
        ext.applyConventionsFor(target)

        target.afterEvaluate {
            if (!target.hasProperty(PROP_USER) || !target.hasProperty(PROP_TOKEN)) {
                target.logger.warn("Skipping repository setup due to missing properties. This is fine in a local environment!")
                return@afterEvaluate
            }

            val qualifier = ext.usedVersionQualifier.get()

            // if the current version is not marked as deployable, there is nothing to do for us
            if (qualifier !in ext.getDeployableVersionQualifiers()) return@afterEvaluate

            target.extensions.findByType(PublishingExtension::class.java)?.repositories { repos ->
                val repositoryUrl = ext.qualifierToRepositoryUrl.get()[qualifier]
                    ?: throw IllegalArgumentException("The qualifier to repository url map contained no value for $qualifier")

                target.logger.lifecycle("Adding maven repository for qualifier '$qualifier' at $repositoryUrl")

                repos.maven {
                    it.name = qualifier.name.lowercase().replaceFirstChar(Char::uppercase)
                    it.url = URI(repositoryUrl)

                    it.credentials { cred ->
                        cred.username = target.property(PROP_USER).toString()
                        cred.password = target.property(PROP_TOKEN).toString()
                    }
                }
            } ?: target.logger.warn("Skipping repository setup because the gradle publishing plugin is not applied.")
        }
    }
}
