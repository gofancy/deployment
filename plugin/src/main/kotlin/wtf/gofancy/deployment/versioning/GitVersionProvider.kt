/*
 * This file is part of Gradle Deployment Plugin, licensed under the MIT License
 *
 * Copyright (c) 2024 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.deployment.versioning

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.ListBranchCommand
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.lib.RepositoryBuilder
import org.gradle.api.logging.Logging
import wtf.gofancy.deployment.model.VersionDescriptor
import wtf.gofancy.deployment.model.VersionQualifier
import java.io.File

class GitVersionProvider(
    private val gitDir: File,
    private val defaultBranches: List<String> = listOf("main", "master"),
) : VersionProvider {

    private val logger = Logging.getLogger(this::class.java)

    data class GitDescription(
        val tag: String,
        val distance: Int,
        val commit: String,
    ) {

        companion object {

            fun parse(description: String): GitDescription {
                val elements = description.split("-")

                return GitDescription(
                    tag = elements.subList(0, elements.size - 2).joinToString("-"),
                    distance = elements[elements.size - 2].toInt(),
                    commit = elements[elements.size - 1].removePrefix("g"),
                )
            }
        }
    }

    override fun createVersionDescriptor(): VersionDescriptor {
        val git = Git.wrap(RepositoryBuilder().findGitDir(this.gitDir).build())

        if (git.repository.resolve(Constants.HEAD) == null) {
            // since repository is empty, we can be sure we are not on a default branch so bleeding is fitting
            this.logger.info("Found empty Git repository, using fallback version.")
            return VersionDescriptor(0, 0, 0, VersionQualifier.BLEEDING, "")
        }

        // from this point on we know the Git repository is not empty, so all helper functions do not need to check that

        val (tag, distance, commit) = this.getDescription(git) ?: run {
            /*
             * The default values chosen here are compatible with the algorithm used below to determine the current version
             * description and will result in version 0.0.0, a qualifier according to the current branch and an identifier
             * according to the selected qualifier.
             */
            this.logger.info("Found no tags in Git repository, using fallback version.")
            GitDescription("0.0.0", -1, this.getHeadSha(git))
        }

        // todo try to find a tag that actually represents a version
        val semVersion = tag.split("-").first().removePrefix("v")

        if (distance == 0) {
            // we are directly on a tag, so this is a release version
            val identifier = tag.split("-").drop(1).joinToString("-")
            return VersionDescriptor(semVersion, VersionQualifier.RELEASE, identifier)
        }

        val branch = this.getContainingBranch(git)

        this.logger.info("Evaluated containing branch to be '$branch'")

        return if (branch in this.defaultBranches) {
            // current branch is one of the configured defaults, so we treat this as a snapshot version
            VersionDescriptor(semVersion, VersionQualifier.SNAPSHOT, commit)
        } else {
            // everything else is a bleeding version
            val identifier = branch?.lowercase()?.replace("/", "-") ?: ""
            VersionDescriptor(semVersion, VersionQualifier.BLEEDING, identifier)
        }
    }

    /**
     * Returns the branch containing the current HEAD, or null if no single branch can be determined.
     *
     * `git branch --all --contains` gets used to obtain a list of branches which contain the current HEAD. If there
     * is only one such branch, it gets returned. Otherwise, the method tries to find a single [default branch][defaultBranches]
     * in the list and returns it. If there are multiple or no default branches, the method returns `null`.
     *
     * The method checks for default branches to solve the simple case where another branch was merged into `main` and
     * the commit now exists on both branches. More complex cases are not tried to be solved, as doing so would require
     * a very opinionated decision. Instead `null` gets returned to indicate to the caller that a default value must be
     * chosen instead.
     *
     * This function assumes that the Git repository it is operating on is not empty (i.e. there exists at least one
     * commit).
     *
     * @return The short name of the branch containing the current HEAD or `null` if no single branch can be determined.
     */
    private fun getContainingBranch(git: Git): String? {
        val list = git
            .branchList()
            .setListMode(ListBranchCommand.ListMode.ALL)
            .setContains(Constants.HEAD)
            .call()
            .asSequence()
            .map(Ref::getName)
            .filterNot { it.equals(Constants.HEAD) }            // on detached head this would be included
            .map { it.removePrefix("refs/heads/") }             // remove prefix of local branches
            .map { it.removePrefix("refs/remotes/origin/") }    // remove prefix of remote branches
            .distinct()

        return list.singleOrNull()
            ?: list.singleOrNull { it in this.defaultBranches }
    }

    /**
     * Returns the [GitDescription] describing the current HEAD.
     *
     * Uses `git describe --long` to obtain the raw description string which is then passed into [GitDescription.parse].
     *
     * This function assumes that the Git repository it is operating on is not empty (i.e. there exists at least one
     * commit).
     *
     * @return The [GitDescription] describing the current HEAD or `null` if the repository has no tags.
     */
    private fun getDescription(git: Git): GitDescription? {
        val desc = git.describe().setLong(true).call() ?: return null

        return GitDescription.parse(desc)
    }

    /**
     * Returns the abbreviated SHA of the current HEAD in the given Git repository.
     *
     * This function assumes that the Git repository it is operating on is not empty (i.e. there exists at least one
     * commit).
     *
     * @return The abbreviated SHA of the current HEAD in the given Git repository.
     */
    private fun getHeadSha(git: Git): String {
        return git.repository.resolve(Constants.HEAD).abbreviate(7).name()
    }
}
