pluginManagement {
    repositories {
        maven {
            name = "Garden of Fancy"
            url  = uri("https://maven.gofancy.wtf/releases")
        }
        maven {
            name = "Garden of Fancy Snapshots"
            url  = uri("https://maven.gofancy.wtf/snapshots")
        }
        gradlePluginPortal()
    }
}

plugins {
    id("com.gradle.develocity") version "3.17.5"
}

develocity {
    buildScan {
        if ("CI" in System.getenv()) {
            // we do not want to agree to the terms of use for others automatically, so only agree on CI environments
            // todo move to self hosted instance
            termsOfUseUrl.set("https://gradle.com/help/legal-terms-of-use")
            termsOfUseAgree.set("yes")
        }
        publishing {
            onlyIf { "CI" in System.getenv() }
        }
        // do not upload in background on CI environments, which may then exit already and break the upload
        uploadInBackground.set("CI" !in System.getenv())
    }
}

/*
 * normally, this directory resides in GRADLE_USER_HOME/caches/build-cache
 * but this means that the build cache is inside the CI cache used for dependencies
 * this would mean that everytime dependencies get updated the build cache also gets cleared
 * instead we move it to a different directory and cache it separately
 *
 * TODO use a remote build cache instead (maybe self hosted), this would mean we do not need to cache it in the CI
 */
buildCache {
    local {
        directory = file(gradle.gradleUserHomeDir.resolve("build-cache"))
    }
}

rootProject.name = "gofancy-deployment"

include("plugin")
